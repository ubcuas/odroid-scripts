# File to be kept on the Desktop of the target Odroid to work with the 'syncing.py' program

import os

# File path to the folder containing all the images from odroid SkyPasta
images_folder_path = "/home/odroid/Desktop/skypasta/Images" # "/home/odroid/Desktop/skypasta/build/flir/Untagged" for flir camera

def choose_image():
    try:
        # Get image with highest instance number and highest shot number -- x: instance, y: shot number
        # xxxxxxxxx-yy.jpg | <- format
        max_instance = -1
        max_shot = -1

        # Find the highest instance number, store it as a string (we may have leading zeroes)
        for file in os.listdir(images_folder_path):
            instance_str = file.split("-")[0]
            instance = int(instance_str)

            if instance > int(max_instance):
                max_instance = instance_str

        # Find the highest shot number, store it as a string (we may have leading zeroes)
        for file in os.listdir(images_folder_path):
            shot_str = file.split("-")[1][:-4]
            shot = int(file.split("-")[1][:-4])

            if shot > int(max_shot):
                max_shot = shot_str

        image_path = "{}/{}-{}.jpg".format(images_folder_path, max_instance, max_shot)
        return image_path

    except Exception as e:
        print("Exception encountered: " + str(e))
        return ""

if __name__ == "__main__":
    print(choose_image())