# To copy files from a local machine to our VPS

"""
hard coded values to change: 
    user
    pkey_path
    remote_path
    local_path
"""

import paramiko
from scp import SCPClient
import os

server = '51.222.12.76'
port = '61653'
user = 'debian'
pkey_path = '/home/sally/.ssh/id_ecdsa'

remote_path = '/home/sally/test_dump'
local_path = ''

pkey = paramiko.ECDSAKey.from_private_key_file(pkey_path)

client = paramiko.SSHClient()

client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))

#client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

client.connect(hostname=server, port=port, username=user, pkey=pkey)


scp = SCPClient(client.get_transport())

#test
scp.put('test.txt', 'test2.txt')


scp.close()