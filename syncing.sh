#!/bin/bash
# Repeatedly copies skypasta images onto your local machine. Run with 'bash syncing.sh' at the location of the file in terminal
# Sometimes line endings get messed up and so the file won't run. You will need to run 'sed -i 's/\r//' syncing.sh' in a Linux terminal to fix this
count=1
while true
do
	# The first file path argument may need to be adjusted depending on what camera is being used
	# The last argument (destination file path) can be changed. Must be relative to the script location
	rsync -a -W --ignore-existing --delete-after -e "ssh -i ~/.ssh/id_rsa -p 33473" odroid@51.222.12.76:/home/odroid/Desktop/skypasta/Images ../../../../../../Desktop/AEACTask1
	if [ $count -eq 1 ]
	then
		echo -n "Initial File Pull"
	else
		# Clear terminal line then print pull count
		echo -ne "\033[2K"
		echo -ne "\rPull $count"
	fi
	((count+=1))
	sleep 0.05
done

# To compile a video with the photos, you must have a linux terminal
# 'cd' to the image folder location. If on Windows and are using WSL then use 'cd /mnt/c' to mount the C: drive and then 'cd' to the folder
# Start by deleting unnecessary photos (waiting to take off/post landing photos if there are a lot)
# Install ffmpeg with 'sudo apt install ffmpeg'
# Run 'ffmpeg -framerate 20 -r 10 -pattern_type glob -i '*.jpg' -vcodec libx264 video.mp4'
# * -r is the output framerate (can be changed to your liking)
# * -framerate is processing framerate (increase if low on time to produce video)