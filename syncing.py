# To copy the latest images from Odroid to a local machine (alternative to rsync) and display them

"""
hard coded values to change: 
    port
    user
    pkey_path
    remote_path
    local_path
"""

import paramiko
from scp import SCPClient
import os
import signal

from tkinter import *
from PIL import ImageTk, Image

# Canvas setup for displaying images
root = Tk()
canvas = Canvas(root, width=640*2, height=480*2)
canvas.pack()

# SSH configurations
server = '51.222.12.76'
port = '33472' # 33472 for remy / 33473 for mark
user = 'odroid'
# Local of SSH key authorized on Odroid (to avoid continual password prompt)
pkey_path = 'C:/Users/Elio/.ssh/id_rsa'

# Path of source folder on Odroid
remote_path = '~/Desktop/skypasta/Images/' # '~/Desktop/skypasta/build/flir/Untagged' for flir camera
# Local download folder (make sure the foler exists)
local_path = 'C:/Users/Elio/Desktop/Images/'

pkey = paramiko.RSAKey.from_private_key_file(pkey_path)

client = paramiko.SSHClient()
client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
# client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname=server, port=port, username=user, pkey=pkey)

scp = SCPClient(client.get_transport())

# Close scp and exit program upon Ctrl+C
def close_scp(signum, frame):
    scp.close()
    exit(0)
 
signal.signal(signal.SIGINT, close_scp)

while True:
    # Execute python file on the Odroid to get the latest image. See 'latest_image.py'
    ssh_stdin, ssh_stdout, ssh_stderr = client.exec_command("python3 ~/Desktop/latest_image.py")
    # Parse the filename output to use later
    image_loc = str(ssh_stdout.read())
    image_loc = image_loc[2:-3]
    out_loc = image_loc.split("/")[6]
    # Make sure the folder isn't empty
    if image_loc.split("/")[6] != "-1--1.jpg":
        # Download the latest image
        scp.get(image_loc, local_path + out_loc)
        # Display that image
        img = ImageTk.PhotoImage(Image.open(local_path + out_loc).resize((1280, 960)))
        canvas.create_image(0, 0, anchor=NW, image=img)
        root.update()
    else:
        print("No images")